﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Password
    {
        public string HashedPassword { get; }
        public string Salt { get; }

        public Password(string hashedPassword, string salt)
        {
            HashedPassword = hashedPassword;
            Salt = salt;
        }
    }
}
