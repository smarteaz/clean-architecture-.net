﻿using Application.Interfaces;
using Domain;
using Infrastructure.EntityFramework_Access.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityFramework_Access.Repositories
{
    public class PasswordRepository : IPasswordRepository
    {
        public void Insert(Guid userId, Password password)
        {
            var pass = new PasswordEntity
            {
                UserId = userId,
                HashedPassword = password.HashedPassword,
                Salt = password.Salt
            };
            //PROCESS
        }
    }
}
