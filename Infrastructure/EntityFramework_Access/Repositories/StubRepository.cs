﻿using Application.Interfaces;
using Domain;
using Infrastructure.EntityFramework_Access.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityFramework_Access.Repositories
{
    public class StubRepository : IStubRepository
    {
        public Guid Insert(Stub stub)
        {
            var entity = new StubEntity
            {
                Name = stub.Name
            };

            //PROCESS

            return Guid.NewGuid();
        }
    }
}
