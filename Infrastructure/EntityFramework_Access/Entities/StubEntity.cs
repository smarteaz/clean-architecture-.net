﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityFramework_Access.Entities
{
    public class StubEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
