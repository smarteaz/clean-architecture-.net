﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityFramework_Access.Entities
{
    public class PasswordEntity
    {
        public Guid UserId { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
    }
}
