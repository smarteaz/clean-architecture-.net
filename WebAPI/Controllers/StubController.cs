﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Boundaries;
using Application.Interfaces.Usecases;
using Application.Usecases;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StubController : ControllerBase
    {
        private IStubRegister _stubRegister;

        public StubController(IStubRegister stubRegister)
        {
            _stubRegister = stubRegister;
        }

        [HttpPost]
        public Guid Post([FromBody] StubInput input)
        {
            return _stubRegister.Execute(input);
        }
    }
}
