﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Boundaries;
using Application.Usecases;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private StubRegister _stubRegister;

        // POST api/values
        [HttpPost]
        public Guid Post([FromBody] StubInput input)
        {
            return _stubRegister.Execute(input);
        }
    }
}
