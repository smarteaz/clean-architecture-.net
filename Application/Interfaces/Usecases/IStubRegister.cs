﻿using Application.Boundaries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces.Usecases
{
    public interface IStubRegister
    {
        Guid Execute(StubInput input);
    }
}
