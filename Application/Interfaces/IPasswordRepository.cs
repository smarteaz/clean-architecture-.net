﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IPasswordRepository
    {
        void Insert(string password, bool withCrypt = false);
    }
}
