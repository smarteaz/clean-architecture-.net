﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Boundaries
{
    public class StubInput
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
