﻿using Application.Boundaries;
using Application.Interfaces;
using Application.Interfaces.Usecases;
using Domain.Kernel.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Usecases
{
    public class StubRegister : IStubRegister
    {
        private IStubRepository _stubRepository;
        private IPasswordRepository _passwordRepository;

        public StubRegister(IStubRepository stubRepository)
        {
            _stubRepository = stubRepository;
        }

        public Guid Execute(StubInput input)
        {
            var id = _stubRepository.Insert(new Domain.Stub
            {
                Name = input.Name
            });

            var password = PasswordHashManager.Hash(input.Password);

            _passwordRepository.Insert(id, password);

            return id;
        }
    }
}
